package com.dezyre;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

public class NASDAQDailyStats {

	public static class MyMapper extends Mapper<LongWritable, Text, Text, LongWritable> {

		@Override
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			String[] tokens= value.toString().split(",");
			if(tokens[0] == "NASDAQ") {
				LongWritable volume = new LongWritable(Long.parseLong(tokens[7]));
				Text theKey = new Text(tokens[1]);
				context.write(theKey, volume);
			}
		}
	}
	
	public static class MyReducer extends Reducer<Text, LongWritable, Text, LongWritable> {

		@Override
		protected void reduce(Text key, Iterable<LongWritable> values, Context context)
				throws IOException, InterruptedException {
			long sum=0L;
			Iterator<LongWritable> valueIterator=values.iterator();
		      while (valueIterator.hasNext()) {
		        sum += valueIterator.next().get();
		      }
		      context.write(key, new LongWritable(sum));
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		Job job = new Job(conf, "nasdaqstats");
		job.setJarByClass(NASDAQDailyStats.class);
		
		job.setMapperClass(MyMapper.class);
		job.setReducerClass(MyReducer.class);
		
		job.setMapOutputKeyClass(Text.class); //stock symbol
		job.setMapOutputValueClass(LongWritable.class); //
			
		job.setOutputKeyClass(Text.class); //stock symbol
		job.setOutputValueClass(LongWritable.class);
	}
}
